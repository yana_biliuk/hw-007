// task 1

function isPalindrome(str) {
    const string = str.split("").reverse().join("");
  //   console.log(string);
    return str === string;
  }
  
  console.log(isPalindrome('level'));
  console.log(isPalindrome('hello'));
  
  
  // task 2
  
  function getResult(str, num) {
  //   return str.length <= num;
  
      if (str.length <= num) {
        return true;
      } else {
        return false;
      }
  }
  
  console.log( getResult("checked string", 20));
  console.log(getResult("checked string", 10));
  
  // task 3
  
  function getAge(birth) {
    const today = new Date();
    const birthDate = new Date(birth);
  
    let age = today.getFullYear() - birthDate.getFullYear();
  
    const monthDiff = today.getMonth() - birthDate.getMonth();
  
    if (
      monthDiff < 0 ||
      (monthDiff === 0 && today.getDate() < birthDate.getDate())
    ) {
      age--;
    }
  
    return age;
  }
  
  const birthDate = prompt("Введіть дату народження у форматі YYYY-MM-DD:");
  
  const userAge = getAge(birth);
  console.log("Ваш вік:", userAge);
  